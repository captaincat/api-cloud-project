<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'firstName' => $faker->firstName,
        'lastName' => $faker->lastName,
        'birthDay' => $faker->dateTimeBetween('-70 years', '-18 years'),
        'lat' => $faker->randomFloat(6, 44, 46),
        'lon' => $faker->randomFloat(6, 4, 5),
    ];
});
