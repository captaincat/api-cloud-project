<?php

namespace Tests\Unit;

use App\User;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserTest extends TestCase
{
    use WithFaker;

    /**
     * Testing that the getAll() function returns all the users in the correct format
     *
     * @return void
     */
    public function testGetAll()
    {
        $userCount = User::count();
        $response = $this->get('/user');

        if ($userCount > 100) {
            $response->assertStatus(200)
                ->assertJsonCount(100);
        } else {
            $response->assertStatus(200)
                ->assertJsonCount($userCount);
        }

        if ($userCount > 0) {
            $response->assertJsonStructure([ $this->getUserJsonStructure() ]);
        }
    }

    /**
     * Testing that the getAllPagination() function returns the first 100 users.
     *
     * @return void
     */
    public function testGetAllPagination()
    {
        $response = $this->get('/user');
        $response->assertStatus(200);
    }

    /**
     * Testing that the store() and get() functions stores and returns the correct user
     *
     * @return void
     */
    public function testStoreAndGet()
    {
        $user = $this->createUser();
        $arrayUser = json_decode($user->toJson(), true);
        $postResponse = $this->post('/user', $arrayUser);
        $arrayUser['id'] = json_decode($postResponse->getContent())->id;

        $response = $this->get('/user/' . $arrayUser['id']);
        $response->assertStatus(200)
            ->assertSee(json_encode($arrayUser)); // Workaround for assertJson bug
    }

    /**
     * Testing that the replaceAll() function replace all the users by the one provided
     *
     * @return void
     */
    public function testReplaceAll()
    {
        $oldUsers = json_decode($this->get('/user')->getContent(), true);
        $newUsers = json_decode($this->createUsers(10)->toJson(), true);

        $putResponse = $this->put('/user', $newUsers);
        $usersFromResponse = json_decode($putResponse->getContent(), true);
        $putResponse->assertStatus(201);

        // Checking that some of the previous users are not part of the response
        foreach (collect($oldUsers)->random(min(count($oldUsers), 5)) as $oldUser) {
            $this->get('/user/' . $oldUser['id'])->assertStatus(404);
        }

        // Checking that the number of users returned is the same as the number of new users
        $this->assertEquals(count($usersFromResponse), count($newUsers));

        // Asserting each new user individually
        for ($i = 0; $i < count($newUsers); $i++) {
            $newUsers[$i]['id'] = $usersFromResponse[$i]['id'];
            $this->assertEquals($newUsers[$i], $usersFromResponse[$i]);
        }
    }

    /**
     * Testing that the update() function updates user's fields
     *
     * @return void
     */
    public function testUpdate()
    {
        // Creating and storing a new user
        $arrayUser = json_decode($this->createUser()->toJson(), true);
        $postResponse = $this->post('/user', $arrayUser);
        $arrayUser['id'] = json_decode($postResponse->getContent())->id;

        // Creating a new user to generate new fields value
        $newUserFields = json_decode($this->createUser()->toJson(), true);
        unset($newUserFields['id']);

        // Getting the stored user
        $this->get('/user/' . $arrayUser['id']);
        // Updating his fields
        $putResponse = $this->put('/user/' . $arrayUser['id'], $newUserFields);
        $newUserFields = array_merge(['id'  => json_decode($putResponse->getContent(), true)['id']], $newUserFields);

        $putResponse->assertStatus(200)
            ->assertSee(json_encode($newUserFields)); // Workaround for assertJson bug
    }

    /**
     * Testing that the delete() function deletes the correct user
     *
     * @return void
     */
    public function testDelete()
    {
        $user = $this->createUser();
        $arrayUser = json_decode($user->toJson(), true);
        $postResponse = $this->post('/user', $arrayUser);
        $user->id = json_decode($postResponse->getContent())->id;

        $response = $this->delete('/user/' . $user->id);
        $response->assertStatus(204);

        $response = $this->get('/user/' . $user->id);
        $response->assertStatus(404);

        $response = $this->delete('/user/' . $user->id);
        $response->assertStatus(500);
    }

    /**
     * Testing that the deleteAll() function deletes all the users
     *
     * @return void
     */
    public function testDeleteAll()
    {
        $response = $this->delete('/user');
        $response->assertStatus(200);
        $this->assertTrue(User::count() == 0);
    }

    /**
     * Creates a single user
     *
     * @return User
     */
    private function createUser()
    {
        return $this->createUsers(1)->first();
    }

    /**
     * Creates several users
     *
     * @param integer   $number     The number of users to create.
     *
     * @return User
     */
    private function createUsers($number)
    {
        $users = collect();

        for ($i = 0; $i < $number; $i++) {
            $user = new User;
            $user->firstName = $this->faker->firstName;
            $user->lastName = $this->faker->lastName;
            $user->birthDay = $this->faker->dateTimeBetween('-70 years', '-18 years');
            $user->lat = $this->faker->randomFloat(6, 44, 46);
            $user->lon = $this->faker->randomFloat(6, 4, 5);
            $users->push($user);
        }

        return $users;
    }

    /**
     * Get the json structure expected for a user
     *
     * @return array
     */
    public function getUserJsonStructure()
    {
        return [
            "id",
            "firstName",
            "lastName",
            "birthDay",
            "position" => [
                "lat",
                "lon"
            ]
        ];
    }
}
