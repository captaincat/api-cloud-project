<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     *  @OA\Get(
     *    path="/user/{id}",
     *    tags={"Users"},
     *    summary="Get a single user by his id",
     *    @OA\Parameter(
     *      name="id",
     *      description="Id of user to return",
     *      required=true,
     *      in="path",
     *      @OA\Schema(
     *        type="string"
     *      )
     *    ),
     *    @OA\Response(
     *      response=200,
     *      description="Successful operation",
     *      @OA\JsonContent(ref="#/components/schemas/User"),
     *    ),
     *    @OA\Response(
     *      response=404,
     *      description="User not found"
     *    )
     *  )
     *
     * Get a single user by his id
     *
     * @param integer   $id     Id of the user.
     *
     * @return mixed
     */
    public function get($id)
    {
        return User::findOrFail($id);
    }

    /**
     *  @OA\Get(
     *    path="/user",
     *    tags={"Users"},
     *    summary="Get all the users",
     *    @OA\Parameter(
     *      name="page",
     *      description="Number of the page to return.",
     *      required=false,
     *      in="query",
     *      @OA\Schema(
     *        type="integer"
     *      )
     *    ),
     *    @OA\Response(
     *      response=200,
     *      description="Successful operation",
     *      @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref="#/components/schemas/User")
     *      )
     *    )
     *  )
     *
     * Get all the users
     *
     * @return User[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getAll()
    {
        if (Input::get('page') != null || User::count() > 100) {
            return $this->getAllPagination();
        }
        return User::all();
    }

    /**
     * Get some users
     *
     * @return array
     */
    public function getAllPagination()
    {
        return $this->pagination(User::all(), Input::get('page'));
    }

    /**
     *  @OA\Delete(
     *    path="/user/{id}",
     *    tags={"Users"},
     *    summary="Delete a single user",
     *    @OA\Parameter(
     *      name="id",
     *      description="Id of user to delete",
     *      required=true,
     *      in="path",
     *      @OA\Schema(
     *        type="string"
     *      )
     *    ),
     *    @OA\Response(
     *      response=204,
     *      description="Successful operation"
     *    ),
     *    @OA\Response(
     *      response=500,
     *      description="Failed to delete"
     *    )
     *  )
     *
     * Delete a single user
     *
     * @param integer $id Id of the user.
     *
     * @throws \Exception   Throws an error if Eloquent fails to delete.
     *
     * @return mixed
     */
    public function delete($id)
    {
        $user = User::find($id);
        if ($user) {
            $user->delete();
            return Response::json([], 204);
        }
        return Response::json([], 500);
    }

    /**
     *  @OA\Delete(
     *    path="/user",
     *    tags={"Users"},
     *    summary="Delete all the users",
     *    @OA\Response(
     *      response=200,
     *      description="Successful operation"
     *    )
     *  )
     *
     * Delete all the users
     *
     * @throws \Exception   Throws an error if Eloquent fails to delete.
     *
     * @return User[]|\Illuminate\Database\Eloquent\Collection
     */
    public function deleteAll()
    {
        User::truncate();
        return $this->getAll();
    }

    /**
     *  @OA\Post(
     *    path="/user",
     *    tags={"Users"},
     *    summary="Store in database the user provided",
     *    @OA\RequestBody(
     *      description="User object that needs to be stored",
     *      required=true,
     *      @OA\JsonContent(ref="#/components/schemas/User")
     *    ),
     *    @OA\Response(
     *      response=201,
     *      description="Successful operation"
     *    ),
     *    @OA\Response(
     *      response=400,
     *      description="Failed to validate data"
     *    )
     *  )
     *
     * Store in database the user provided
     *
     * @param Request $request
     *
     * @return User|\Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validator = $this->validateUser($request->all(), true);

        if ($validator->passes()) {
            $user = DB::table('users')->insertGetId($this->flatten([$request->all()])[0]);
            return Response::json(User::find($user), 201);
        }
        return Response::json([], 400);
    }

    /**
     *  @OA\Put(
     *    path="/user/{id}",
     *    tags={"Users"},
     *    summary="Update the user provided",
     *    @OA\Parameter(
     *      name="id",
     *      description="Id of user to update",
     *      required=true,
     *      in="path",
     *      @OA\Schema(
     *        type="string"
     *      )
     *    ),
     *    @OA\RequestBody(
     *      description="User objects that needs to be stored",
     *      required=true,
     *      @OA\JsonContent(ref="#/components/schemas/User")
     *    ),
     *    @OA\Response(
     *      response=200,
     *      description="Successful operation"
     *    ),
     *    @OA\Response(
     *      response=400,
     *      description="Failed to validate data"
     *    ),
     *    @OA\Response(
     *      response=404,
     *      description="User not found"
     *    )
     *  )
     *
     * Update the user provided
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $user = User::findOrFail($request->id);
        $validator = $this->validateUser($request->all());

        if ($validator->passes()) {
            return Response::json($this->updateUser($user, $this->flatten([$request->all()])[0]));
        }
        return Response::json([], 400);
    }

    /**
     *  @OA\Put(
     *    path="/user",
     *    tags={"Users"},
     *    summary="Remove all the existing users and stores the ones provided",
     *    @OA\RequestBody(
     *      description="User objects that needs to be stored",
     *      required=true,
     *      @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref="#/components/schemas/User")
     *      )
     *    ),
     *    @OA\Response(
     *      response=201,
     *      description="Successful operation"
     *    ),
     *    @OA\Response(
     *      response=400,
     *      description="Failed to validate data"
     *    )
     *  )
     *
     * Replace all the users by the ones provided
     *
     * @param Request $request
     *
     * @throws \Exception   Throws an error if Eloquent fails to delete all the existing users.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function replaceAll(Request $request)
    {
        $this->deleteAll();
        DB::table('users')->insert($this->flatten($request->all()));
        return Response::json($this->getAll(), 201);
    }

    /**
     * Unnest position attributes from a Json
     *
     * @param array $items
     *
     * @return array
     */
    private function flatten(array $items)
    {
        $result = [];
        foreach ($items as $item) {
            $currentItem = [];
            if (isset($item['firstName'])) {
                $currentItem['firstName'] = $item['firstName'];
            }
            if (isset($item['lastName'])) {
                $currentItem['lastName'] = $item['lastName'];
            }
            if (isset($item['birthDay'])) {
                $currentItem['birthDay'] = $item['birthDay'];
            }
            if (isset($item['position']) && isset($item['position']['lat'])) {
                $currentItem['lat'] = $item['position']['lat'];
            }
            if (isset($item['position']) && isset($item['position']['lon'])) {
                $currentItem['lon'] = $item['position']['lon'];
            }

            $result[] = $currentItem;
        }

        return $result;
    }

    /**
     * Updates the fields of a user
     *
     * @param User  $user   The user to update.
     * @param array $data   The data to update the user with.
     *
     * @return User
     */
    private function updateUser($user, $data)
    {
        if (isset($data['firstName'])) {
            $user->firstName = $data['firstName'];
        }
        if (isset($data['lastName'])) {
            $user->lastName = $data['lastName'];
        }
        if (isset($data['birthDay'])) {
            $user->birthDay = Carbon::createFromFormat('m/d/Y', $data['birthDay'])->format('Y-m-d');
        }
        if (isset($data['lat'])) {
            $user->lat = $data['lat'];
        }
        if (isset($data['lon'])) {
            $user->lon = $data['lon'];
        }
        $user->save();

        return $user;
    }

    /**
     * Returns a validator instance checking if the data provided is correct
     *
     * @param array     $data       The data to validate.
     * @param boolean   $required   Are all the fields required.
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    private function validateUser($data, $required = false)
    {
        $requiredString = '';
        if ($required) {
            $requiredString = 'required|';
        }
        $rules = [
            'firstName' => $requiredString . 'string',
            'lastName' => $requiredString . 'string',
            'birthDay' => $requiredString . 'date_format:m/d/Y',
            'position.lat' => $requiredString . 'numeric',
            'position.lon' => $requiredString . 'numeric'
        ];
        $validator = Validator::make($data, $rules);

        return $validator;
    }

    /**
     *  Paginates a collection
     *
     * @param mixed     $items      Array or collection of items to paginate.
     * @param integer   $page       Current page.
     * @param integer   $perPage    Number of items to return.
     *
     * @return array
     */
    public function pagination($items, $page = null, $perPage = 100)
    {
        $page = $page >= 1 ? $page + 1 : $page;
        $items = $items instanceof Collection ? $items : Collection::make($items);
        if ($items->forPage($page, $perPage)->isEmpty()) {
            return null;
        }
        return $items->forPage($page, $perPage)->toArray();
    }
}
