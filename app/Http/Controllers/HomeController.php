<?php

namespace App\Http\Controllers;

class HomeController extends Controller
{
    /**
     * Return the homepage
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('welcome');
    }
}
