<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Artisan;

class DebugController extends Controller
{
    /**
     * @OA\Get(
     *    path="/debug/seed",
     *    tags={"Maintenance"},
     *    summary="Seed the database with fake data",
     *    @OA\Response(
     *      response=200,
     *      description="Successful operation",
     *      @OA\JsonContent(ref="#/components/schemas/User"),
     *    )
     *  )
     *
     * Seed the database with fake data.
     *
     * @return mixed
     */
    public function seed()
    {
        Artisan::call('db:seed');
        return User::all();
    }
}
