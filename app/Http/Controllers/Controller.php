<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Route;
use League\StatsD\Laravel5\Facade\StatsdFacade;

/**
 * @OA\Info(
 *      version="1.0.0",
 *      title="Cloud API",
 *      description="This page describes and allows to test the endpoints of our API."
 * )
 */
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Execute an action on the controller.
     *
     * @param  string  $method
     * @param  array   $parameters
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function callAction($method, $parameters)
    {
        // Overriding callAction parent function to measure time
        $timerName = sprintf('routes.%s.%s', Route::getCurrentRoute()->uri(), $method);
        $timer = StatsdFacade::startTiming($timerName);
        $response = parent::callAction($method, $parameters);
        $timer->endTiming($timerName);

        return $response;
    }
}
