<?php

namespace App\Http\Middleware;

use Closure;
use League\StatsD\Laravel5\Facade\StatsdFacade;

class StatsdMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        StatsdFacade::increment('totalViewCount');
        return $next($request);
    }
}
