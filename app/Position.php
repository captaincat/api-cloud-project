<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Model;

/**
 * @OA\Schema(
 *     schema="Position",
 *     type="object",
 *     @OA\Property(property="lat", type="number", example="45.427906"),
 *     @OA\Property(property="lon", type="number", example="4.40054")
 * )
 */
class Position extends Model
{
    /**
     * Get the user associated to this position.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne('App\User');
    }

    /**
     * Defines the array returned when serialized
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'lat' => $this->lat,
            'lon' => $this->lon
        ];
    }
}
