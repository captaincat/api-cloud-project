<?php

namespace App;

use Carbon\Carbon;
use Jenssegers\Mongodb\Auth\User as Authenticatable;
use MongoDB\BSON\ObjectId;

/**
 * @OA\Schema(
 *     schema="User",
 *     type="object",
 *     @OA\Property(property="id", type="integer", example="5c28ffdffdce2709ba144238", readOnly="true"),
 *     @OA\Property(property="firstName", type="string", example="John"),
 *     @OA\Property(property="lastName", type="string", example="Doe"),
 *     @OA\Property(property="birthDay", type="string", example="02/17/1995"),
 *     @OA\Property(property="position", ref="#/components/schemas/Position")
 * )
 */
class User extends Authenticatable
{
    /**
     * Indicates if the model should store timestamps
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = 'm/d/Y';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'birthDay'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstName', 'lastName', 'birthDay', 'lat', 'lon'
    ];

    /**
     * Defines the array returned when serialized
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'id' => $this->id,
            'firstName' => $this->firstName,
            'lastName' => $this->lastName,
            'birthDay' => $this->birthDay->format('m/d/Y'),
            'position' => [
                'lat' => $this->lat,
                'lon' => $this->lon
            ]
        ];
    }

    /**
     * Get the position record associated with the user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function position()
    {
        return $this->belongsTo('App\Position');
    }

    /**
     * Custom accessor for the model's id.
     *
     * @param  mixed $value
     *
     * @return mixed
     */
    public function getIdAttribute($value = null)
    {
        // If we don't have a value for 'id', we will use the Mongo '_id' value.
        // This allows us to work with models in a more sql-like way.
        if (array_key_exists('_id', $this->attributes)) {
            $value = $this->attributes['_id'];
        }

        // Convert ObjectID to string.
        if ($value instanceof ObjectID) {
            return (string) $value;
        }
        return $value;
    }
}
