FROM php:7
RUN apt-get update -y && apt-get install -y openssl zip unzip git libcurl4-openssl-dev pkg-config libssl-dev
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN docker-php-ext-install pdo mbstring
RUN pecl install mongodb
RUN echo "extension=mongodb.so" > /usr/local/etc/php/php.ini
WORKDIR /app
COPY . /app
RUN composer install
RUN cp .env.docker .env && php artisan key:generate && php artisan config:cache

CMD php artisan serve --host=0.0.0.0 --port=8181
EXPOSE 8181
