# Projet API Cloud

## Installation sur Clever Cloud

- Créer une application sur Clever Cloud
  - Utiliser le preset PHP ou Docker
- Obtenir des tokens de connexion avec Clever Tools
- Mettre les tokens et le nom de l'app dans les variables CI de Gitlab
- Lancer un pipeline sur la branche master

## Installation en local avec Docker

- Installer docker.io et docker-compose
- `docker-compose up`
