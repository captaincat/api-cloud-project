<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user/{id}', 'UserController@get');
Route::get('/user', 'UserController@getAll');

Route::post('/user', 'UserController@store');

Route::put('/user/{id}', 'UserController@update');
Route::put('/user', 'UserController@replaceAll');

Route::delete('/user/{id}', 'UserController@delete');
Route::delete('/user', 'UserController@deleteAll');

Route::get('/debug/seed', 'DebugController@seed');
